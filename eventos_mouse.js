$(() => {
    
    //alternativo|| e-> retorno da função scroll
    $('#div1').scroll( e => {					
        $(e.target).css('background-color','blue')
    })

    //capturado ao pressionar o botão do mouse
    $('#btn1').mousedown(()=>{
        $('#div2').css('background-color','green')
    })		

    //capturado ao soltar o botão do mouse - similar ao evento click[$('#btn1').click(()=>{....]
    $('#btn1').mouseup(()=>{
        $('#div2').css('background-color','yellow')
    })

    //capturando ao ser realizado um duplo click	
    $('#btn2').dblclick(()=>{
        $('#div2').css('background-color','purple')
    })	

    //proporciona o efeito de arrastar elementos dentro da pagina
    //captura a posicão do mouse sobre a pagina
    $('#div2').mousemove((e)=>{		
        $('#resultado').html('X: '+e.offsetX+', Y:'+e.offsetY)

    })

    //ativado o quando o mouse entra no 'territorio' do elemento
    $('#divExterna').mouseenter(()=>{
        $('#resultadoDiv1').html('entrou...')
        console.log('Div Externa: (entrou)')
    })	

     //ativado o quando o mouse sai do 'territorio' do elemento
    $('#divExterna').mouseleave(()=>{
        $('#resultadoDiv1').html('saiu...')
        console.log('Div Externa: (saiu)')
    })

    //executa as duas funções acima
    $('table thead tr').hover(function(){
        $(this).addClass('colorado');
        }, function(){
            $(this).removeClass('colorado');
        }) 
    });


    //ativado ao clicar com botão direito
    $('img').contextmenu(function(){
        console.log('clicou com o botão direito')
    });


    //forma recomendada de usar eventos
    $(".btn").on("<nome_evento>", function(){
        
    })
})