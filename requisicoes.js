$(() => {
    /*
    LOAD
    # $.load(url [,data][,complete]) -> Carrega dados do servidor e coloca o HTML retornado nos elementos correspondentes
    # É aproximadamente equivalente a $ .get (url, dados, sucesso), exceto que é um método em vez de uma função global e tem uma função de retorno de chamada implícita
    # O método POST é usado se os dados forem fornecidos como um objeto; caso contrário, GET é assumido.
    */
    
    $.load(url ,[data],[complete])
    /*
    rl  -> String contendo a URL na qual requisição é enviada
    data -> um objeto plano({}, {'nome':'vaness'}) ou uma string para ser enviada ao servidor como solicitação
    complete -> function(string responseText, String TextStatus, jqXHR jqXHR) função que é executada quando a solicitação é completada
    */

    //Se nenhum elemento for correspondido pelo seletor - neste caso, se o documento não contiver um elemento com id = "resultado" - a solicitação Ajax não será enviada.
    $( "#result" ).load( "ajax/test.html" );

    //Se um retorno de chamada "completo" for fornecido, ele será executado após o pós-processamento e a inserção do HTML. O retorno de chamada é disparado uma vez para cada
    //elemento na coleção jQuery e é definido para cada elemento DOM por vez.
    $( "#result" ).load( "ajax/test.html", function() {
        alert( "Load was performed." );
    });
    
    /* O método .load (), ao contrário de $ .get (), nos permite especificar uma parte do documento remoto a ser inserido. Isso é obtido com uma sintaxe especial para
    o parâmetro url. Se um ou mais caracteres de espaço forem incluídos na string, a parte da string após o primeiro espaço é considerada um seletor jQuery que determina
    o conteúdo a ser carregado.*/
    $( "#result" ).load( "ajax/test.html #container" );
    //Quando este método é executado, ele recupera o conteúdo de ajax / test.html, mas então jQuery analisa o documento retornado para encontrar o elemento com um ID de container.
    //Este elemento, junto com seu conteúdo, é inserido no elemento com um ID de resultado, e o resto do documento recuperado é descartado.
    //----------------------------------------------------------------------------------------------------------------



    /*
    GET
    # $.get( url [, data ] [, success ] [, dataType ]) -> Carrega os dados do servidor usando requisição HTTP GET
    # A função de retorno de chamada de sucesso recebe os dados retornados, que serão um elemento raiz XML, string de texto, arquivo JavaScript ou objeto JSON, dependendo do tipo
     MIME da resposta. Também é passado o status de texto da resposta.
    # 
    */
   $.get( url, [ data ], [ success ], [ dataType ] )
   /*
   url     -> String contendo a URL na qual requisição é enviada 
   data    -> um objeto plano({}, {'nome':'vaness'}) ou uma string para ser enviada ao servidor como solicitação
   success -> Function( PlainObject data, String textStatus, jqXHR jqXHR ) -- Uma função de retorno de chamada que é executada se a solicitação for bem-sucedida.
   Obrigatório se dataType for fornecido, mas você pode usar null ou jQuery.noop como um espaço reservado.
   dataType-> O tipo de dados esperado do servidor. Padrão: Intelligent Guess (xml, json, script, texto, html).
   */
    
   //Pode ser passado usando Plain Object, todos os parametros são opcionais, exceto url
   $.get({
    url: url,
    data: data,
    success: success,
    dataType: dataType
    });
   //equivalente ao $.get()
    $.ajax({
        url: url,
        data: data,
        success: success,
        dataType: dataType
    });

    /*
    A interface Promise também permite que os métodos Ajax da jQuery, incluindo $ .get (), encadear vários retornos de chamada .done (), .fail () e .always () em uma única
    solicitação e até mesmo atribuir esses retornos de chamada após a solicitação. Completou. Se a solicitação já estiver concluída, o retorno de chamada será acionado imediatamente.
    */
    var jqxhr = $.get( "example.php", function() {
    alert( "success" );
    })
    .done(function() {
      alert( "second success" );
    })
    .fail(function() {
      alert( "error" );
    })
    .always(function() {
      alert( "finished" );
    });
   

    //Obtenha o conteúdo da página test.php, que foi retornado no formato json (<? Php echo json_encode (array ("name" => "John", "time" => "2pm"));?>) E adicione para a página.
    //passando a url e success
    $.get( "test.php", function( data ) {
        $( "body" )
          .append( "Name: " + data.name ) // John
          .append( "Time: " + data.time ); //  2pm
    }, "json" );
    //--------------------------------------------------------------------------------------------------------------------------------------------


    /*
    POST
    $.post( url [, data ] [, success ] [, dataType ] ) -> Envia dados ao servidor usando requisição HTTP POST

    */
   jQuery.post( url [, data ] [, success ] [, dataType ] )
    /*
   url     -> String contendo a URL na qual requisição é enviada 
   data    -> um objeto plano({}, {'nome':'vaness'}) ou uma string para ser enviada ao servidor como solicitação
   success -> Function( PlainObject data, String textStatus, jqXHR jqXHR ) -- Uma função de retorno de chamada que é executada se a solicitação for bem-sucedida.
   Obrigatório se dataType for fornecido, mas você pode usar null ou jQuery.noop como um espaço reservado.
   dataType-> O tipo de dados esperado do servidor. Padrão: Intelligent Guess (xml, json, script, texto, html).
   */

    //Pode ser passado usando Plain Object, todos os parametros são opcionais, exceto url
    $.post({
    url: url,
    data: data,
    success: success,
    dataType: dataType
    });
    //equivalente ao $.post()
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: success,
        dataType: dataType
    });

  //A função de retorno de chamada de sucesso recebe os dados retornados, que serão um elemento raiz XML ou uma string de texto, dependendo do tipo MIME da resposta. 
  //Também é passado o status de texto da resposta.
  $.post( "test.php", { name: "John", time: "2pm" })
  .done(function( data ) {
    alert( "Data Loaded: " + data );
  });

  $.post( "test.php", { func: "getNameAndTime" }, function( data ) {
    console.log( data.name ); // John
    console.log( data.time ); // 2pm
  }, "json");
  //--------------------------------------------------------------------------------------------------------------------------------

});