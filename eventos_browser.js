$(() => {
    $('#onload').html('Página carregada')

    $(window).scroll(()=>{
        $('#scroll').html('Scroll acionado')
    })

    $(window).resize(()=>{
        $('#resize').html('Pagina redimensionada')
    })


    //function / arrow function
    $('#div1').scroll(function(){
        //como queremos selecionar o proprio elemento que foi alvo da captura do evento ('#div1'), podemos utilizar a notação 'this'
        $(this).css('background-color','blue')
    })

    /*alternativo|| e-> retorno da função scroll
    $('#div1').scroll(e =>{
        //como queremos selecionar o proprio elemento que foi alvo da captura do evento (e.target), podemos utilizar a notação 'this'
        $(this).css('background-color','blue')
    })*/
})