$(() => {
    //DELEGAÇÃO DE EVENTOS
    //A obrigação de listen ficou por conta do 'tbody', porém quem vai ser executar o evento é o td
    //dessa forma caso inserimos um novo 'td' de forma dinamica, este automaticamente também podera
    //excutar um evento, caso seja clicado
    $('#books_list tbody').on('click','td', function(e){
        if(e.type == 'contextmenu') e.preventDefault();

        $('#books_list tbody td').css({backgroundColor: 'initial', color:'initial'});
        $(this).css({backgroundColor: 'black', color:'white'});
     });


    //e.delegateTarget seleciona o elemento que tem a responsabilidade de ficar atento aos eventos,
    //neste caso 'tbody', depois pedimos para encontra todos os 'tr', e pedimos para remover a classe 
    $(e.delegateTarget).find('tr').removeClass('trselected');
    $('#books_list tbody').on('click','tr', function(e){
    $(this).addClass('trselected');  
    });

    //Parando uma propagação de evento 
    $('#form_login [name=bio]').on('change', function(e){
        e.stopPropagation()
        $(this).css('background','yellow');
    });


})