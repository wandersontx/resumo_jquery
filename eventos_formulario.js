$(() => {

    $('#nome').focus((e)=>{
        console.log('Esse elemento html recebeu o foco (nome)')
        //target -> alvo do evento
        $(e.target).addClass('inputFocado')
    })

    //funciona apenas diretamente no elemento
    $('#nome').blur((e)=>{
        console.log('Esse elemento perdeu foco (nome_)')
        $(e.target).removeClass('inputFocado')
    })

    $('#opcao').change((e)=>{
        console.log('A opção foi modificada: ('+$(e.target).val()+')')

    })

    //aplica também em elementos filho
    $('.form-group').focusin(function(){
        console.log('você selecionou um campo')
    });

    $('.form-group').focusout(function(){
         console.log('você deixou um campo')
    });

    //Não identifica teclas especiais, poreḿ diferencia 'A' de 'a'
    $('#form_login [name=username]').keypress(function(event){
        console.log(`código  ${event.keyCode}`)
    });

    //seleciona textos
    $('[type=text]').select(function(){
        console.log(`selecionado: ${$(this).val()}`)
    });


    $('#form').submit((e)=>{
        //evita o comportamento padrão do formulário
        e.preventDefault()
        console.log('Formulário enviado...')

    })
});