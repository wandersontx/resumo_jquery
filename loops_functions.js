$(() => {
  
//# $.each -> função generica de interação
//$.each(array, callback) -> interando a partir de um arrat
$.each([ 52, 97 ], function( index, value ) {
alert( index + ": " + value );
})

//$.each(object, callback) -> interando a partir de um objeto
var obj = {
"flammable": "inflammable",
"duh": "no duh"
};
$.each( obj, function( key, value ) {
  alert( key + ": " + value );
});

//interando um checkox - checkgox geram um array de elementos
$.each($('.interesse:checked'),(indice, valor) =>{
  console.log(indice, valor.value)
})

//Realiza a interação sobre um objeto JQuery
$("seletor").each(function(){});

//this fazendo referencia ao li do index sendo iterado pelo array e recuperando o seu texto
$( "li" ).each(function( index ) {
  console.log( index + ": " + $( this ).text() );
});
    
//aplicando uma classe a cada iteração
$( "li" ).each(function() {
  $( this ).addClass( "foo" );
});
//----------------------------------------------------------------------------------

//$.map(array, callback) -> retorna todos itens de um array ou objeto em um novo array de itens
//passano um array
$.map( [ 0, 1, 52, 97 ], function( a ) {
  return (a > 50 ? a - 45 : null);
});
//return [ 7, 52 ]

//passando um objeto
var dimensions = { width: 10, height: 15, length: 20 };
var keys = $.map( dimensions, function( value, key ) {
  return key;
});
//result [ "width", "height", "length" ]


//utilizando o map em objeto JQuery
//Como retorna um objeto JQuery no qual contem um array,  é comum chamar o metodo
//.get() no resultado para trabalhar com um array
$( ":checkbox" )
  .map(function() {
    return this.id;
  })
  .get()
  .join();
  //return "two,four,six,eight"


  
})