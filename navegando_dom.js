$(() => {

    //seleciona o primeiro elemento pai
    $(".secao").parent();

    //Usado para obter os ancestrais do elemento selecionado.
    $(".secao").parents();

    //usado para obter todos os ancestrais, mas não incluindo o elemento correspondido pelo seletor.
    $(".secao").parentsUntil("html")

    //método é usado para obter os filhos diretos do elemento selecionado
    $("ul").children()

    //usado para obter os elementos irmãos do elemento selecionado.
    $("p").siblings()

    //método é usado para obter o irmão imediatamente seguinte, ou seja, o próximo elemento irmão do elemento selecionado.
    $("p").next()

    //usado para obter todos os irmãos seguintes do elemento selecionado
    $("p").nextAll()

    //método é usado para obter o irmão imediatamente anterior, ou seja, o elemento irmão anterior do elemento selecionado
    $("ul").prev()

    //usado para obter todos os irmãos anteriores do elemento selecionado.
    $("ul").prevAll()

    //usado para obter todos os irmãos anteriores, mas não incluindo o elemento correspondido pelo seletor.
    $("ul").prevUntil("h1")

    //método é usado para obter todos os irmãos a seguir, mas não incluindo o elemento correspondido pelo seletor
    $("h1").nextUntil("ul")

    //Procura por um elemento pai, retornando a primeira ocorrência encontrada
    $(".subsecao").closest(".container");

    //procura por um elemento filho, retornando a primeira ocorrênciaa encontrada
    $("#pagina").find(".secao2")

    //Combinando metodos de busca
    //1º Sobe para o elemento pai
    //2º Desce até encontra o elemento h1
    $('.secao1').parent().find('h1')    

});