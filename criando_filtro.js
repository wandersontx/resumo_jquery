$(() => {
    /*
    data-ano -> 'data'->atributo que permite armazenar informações, como no exemplo 
    estamos armazenando informações referentea ao ano

    -- Um Jquery é um objeto que possui propriedades e metodos
    */

    //criando um filtro no JQuery
    $.expr[':'].carsCentury21 = function(el) {
        
        return ($(el).data('ano') > 2000 );
    } 
    
    //Utilizando o filtro criado
    $('.cars_list li:carsCentury21').css('background-color', 'orange');

    //criando um filtro com parametro
    $.expr[':'].carsBeforeYear = $.expr.createPseudo(function(year){

        return function(el) {
            return ($(el).data('ano') < year);
        }
    });
    //utlizando o filtro com paramentro
    $('.cars_list li:carsBeforeYear(1950)').css('background-color', 'pink');

})