$(() => {
    
    //ocultando um elemento
    $("#div1").hide();
    $("#div1").hide("slow");
    $("#div1").hide("fast");
    $("#div1").hide(3000);

    //exibindo um elemento
    $("#div1").show();
    $("#div1").show("slow");
    $("#div1").show("fast");
    $("#div1").show(3000);

    //alternando entre elementos
    $("#div1").toggle();
    $("#div1").toggle("slow");
    $("#div1").toggle("fast");
    $("#div1").toggle(3000);
    //-----------------------------------------------------



    //--- Opacidade dos elementos

    //fazendo ficar invisivel
    $("#div1").fadeOut()
    $("#div1").fadeOut("slow")
    $("#div1").fadeOut("fast")
    $("#div1").fadeOut(2500)

    //fazendo ficar visivel
    $("#div1").fadeIn()
    $("#div1").fadeIn("slow")
    $("#div1").fadeIn("fast")
    $("#div1").fadeIn(2500)

    //alternando entre fadeOut e fadeIn
    $("#div1").fadeToggle()
    $("#div1").fadeToggle("slow")
    $("#div1").fadeToggle("fast")
    $("#div1").fadeToggle(2500)

    //levando ao ponto especifico de opacidade
    $("#div1").fadeTo("slow", 0.5)
    $("#div1").fadeTo("fast", 0.5)
    $("#div1").fadeTo(3000, 0.5)
    //----------------------------------------------------------


    //A div desparece de baixo para cima
    $("#div3").slideUp("slow")
    $("#div3").slideUp("fast")
    $("#div3").slideUp(4000)

    //A div aparece de cima para baixo
    $("#div3").slideDown("slow")
    $("#div3").slideDown("fast")
    $("#div3").slideDown(4000)

    //alterna entre slideUp e slideDown
    $("#div3").slideToggle("slow")
    $("#div3").slideToggle("fast")
    $("#div3").slideToggle(4000)
    //---------------------------------------------------------

    //desabilita animações
    $.fx.off = true; 



    /*
    # Animate
    - atributos em camelCase
    - somente atributos que trabalham com valores númericos
    */

    //Animação sempre envolve uma efeito especial e um tempo determinado
    $('button').on('click', ()=>{
        //1param: estado desejado
        //2param: modo de transição
        $('div').animate({
            'width':'160px',
            'height':'160px',
            'margin-left':'200px',
            'margin-top':'200px',
            'border-radius':'100px'
        },2000) // milissegundos - 'slow' - fast
    })

    $('.cars_list li:has(span)').animate({
        fontSize: '30px',
        opacity: .7
    }, 10000)

    //adiciona 10px de espaço de a cada palavra
    $('.course_item h4').on('click', function(){
        $(this).animate({ wordSpacing:'+=10px'},1500);
    });

    ////adiciona 5px de espaço entre as palavras
    $('footer p').on('click', function(){
        $(this).animate({letterSpacing:'+=5px'},1500, function(){
            console.log('aumentou o espaçamento das letras')
        });
     });

     //animações de sequencia
     $('#init_effect').on('click', function(){
        $('.logo').animate({
            marginTop: '+=90',
        })
        .delay(2000)
        .animate({
            marginLeft: '+=300'
        })
        .queue(function(){
            $('h1').text('animação concluida').css({'color':'green'});
            $(this).dequeue();
        })
        .fadeOut(2000);
    });

    //parando animações
    $('#stop_effect').on('click', function(){
        //stop() ->para animação atual | stop(true) ->para todas animações
        //stop(true,true)->animação pare no destino final da animação especifica
        //$('.logo').stop(true,true);

        //o elemento vai para a posição final programada pelas animações
        $('.logo').finish();
    });

})