$(() => {

    /*
    attr() >> para obter o valor do atributo de um elemento ou definir um ou mais atributos para o elemento selecionado.
    */
    //recuperando valor do atribuo
    $("img").attr("src")

    //altera o valor do atributo
    $("img").attr("src","assets/images/nova_imagem.png")

    //removendo um atributo
    $("h1").removeAttr("title")
    //--------------------------------------------------------------

    /*
    val() >> usado principalmente para obter ou definir o valor atual dos elementos do formulário HTML, como <input>, <select> e <textarea>.
    */
    //recuperando valor do formulário
    $('form [type=text]:first').val()
    $('#nome').val()

    //inserindo valor no formulario
    $('form [type=text]:first').val('Leticia silva')
    $('#origem').val('Facebook')
    //----------------------------------------------------------------


    //adicionando uma propriedade
    $('h1').prop('nome', 'titulo')

    //recuperando a propridade
    $('h1').prop('nome')

    //removendo a propriedade criada. OBS: SÓ PODEMOS REMOVER PROPRIEDADE CRIADA POR NÓS
    //PROPRIEDADES COMO DISABLE, NÃO PODEM SER REMOVIDAS
    $('h1').removeProp('nome')
    //---------------------------------------------------------------------
    
    /*
     text() >> O método é usado para obter o conteúdo de texto combinado dos elementos selecionados, incluindo seus descendentes, ou definir o
     conteúdo de texto dos elementos selecionados.
    */
    //retorna o texto da tag. text() -> trabalha também com xml
    $('header .menu li:eq(1)').text() 

    //alterando texto
    $('header .menu li:eq(1) a').text('Dinossauro') 


    /*
     html() >> é usado para obter ou definir o conteúdo HTML dos elementos.
    */
    //recuperando conteudo interno
    $('#div1').html()

    //setando valores. Podendo até inserir novas tags 
    $('#div1').html('<strong>Vou modificar o perfil</strong>')
    $('#div2').html('Vou deixar somente PHP e Java')
    //-----------------------------------------------------------------------


    //recupera dimensões
    $("#elemento").width()
    $("#elemento").height()

    //setando dimensões
    $("#elemento").width(300)
    $("#elemento").height(800)

    //alterando o atributo ano criando a partir do objeto data no DOM
    $('.cars_list li:first').data('ano', '1990')

    
    //OFFSET -> retorna a posição do elemnto baseado na tag html (ou seja no documento inteiro)
    //retorna a posição x(horizontal) y(vertircal) do elemento
    $('form .field:eq(1)').offset()

    //mudandos a posição do elemento
    $('form .field:eq(1)').offset({
        top : 40,
        left: 300
    })


    //position -> retorna a posição do elemento baseado no seu elemento pai. Não permite definir uma

    //retorna a posição do elemento 
    $('form .field:eq(1)').position()








});