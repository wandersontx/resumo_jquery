$(() => {
    /* $.fn -> está ligado a todos os metodos, como, css(), addClass...
    também permite inserir novos metodos.
    */

    //criando um novo metodo (ou plugin) # Isso permite que funcionalidades 
    //repetitivas podem ser acompladas em metodo/plugin(como é chamado no Jquery)
    $.fn.changeText = function() {
        this.css('font-size', '50px');
        //retornando o objeto para permite uma fila de ações
        return this;
    };
    //-------------------------------------------------------------------------------------------------

    (function($){
        $.fn.load = function (action= 'load', params) {
            let defaults = {
                divClass: 'dual-ring',
                divQtd: 0,
            };
            let options = $.extend(defaults, params);
            return this.each(function() {
                if (action == 'load') {
                    let divToAppend = document.createElement('div');
                    divToAppend.id = 'loading';
                    $(divToAppend).css({
                        'width':'100%', 
                        'height':'100%',
                        'position': 'fixed',
                        'top': 0,
                        'left': 0,
                        'text-align': 'center',
                        'background-color': 'aqua',
                    });

                    let divLoad = document.createElement('div');
                    divLoad.className ='lds-'+options.divClass;
                    divLoad.id = 'load';
                    $(divLoad).css({
                        position: 'absolute',
                        top: '50%',
                        left:' 50%',
                    });

                    for(let i= 1; i <= options.divQtd; i++) {
                        $(divLoad).append('<div></div>');
                    }

                    $(divToAppend).append(divLoad);

                    $(this).append(divToAppend);
                } else {
                    $('#loading').fadeOut(4000,function(){
                        $(this).remove()
                    });
                }
                
            });
        }
    })(jQuery)//Garante que $ receberá um objeto JQuery evitando assim possiveis conflitos

    /*
    divQtd -> Foi tirado a partir da quantidade de divs que
    existem na parte do html em cada plugin, pelo site
    https://loading.io/css/

    Parametros usados nos testes
    $('body').load('load',{
    divClass: 'roller ',
    divQtd: 3    
    })

    $('body').load('load',{
    divClass: 'hourglass',    
    })*/
});