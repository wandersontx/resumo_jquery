$(() => {

    //innerHTML inserção dentro da tag html
    //inserindo um novo item na ultima posicao, lista1
    $('#lista1').append('<li>Item 4</li>')

    //inserindo um novo item na primeira posicao, lista1
    $('#lista1').prepend('<li>Item 0</li>')

    //inserindo antes do elemento selecionado
    $('#lista1').before('<h1>JQuery</h1>')

    //insenrindo uma tag apos a lista1
    $('#lista1').after('<strong><p>Criando mais um elemento</p></strong>')

    //substituindo uma tag
    $('#lista2').html('<textarea>Novo elemento subtitudo</textarea>')

    //removendo um elemento
    $('#lista3 .rem').remove()

    // retorna um objeto JQuery com objeto clonado
    let el = $('.title').clone()

    //retorna o objeto e seus eventos associados
    let el = $('.title').clone(true)

    //substituindo elemento
    $("elemento").replaceWith("tag")

});