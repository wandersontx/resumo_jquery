$(() => {
    /*
  AJAX
  >> jQuery.ajax( url [, settings ] ) -> executa uma requisição assincrona HTTP

  >> A função $ .ajax () é a base de todas as solicitações Ajax enviadas por jQuery. Freqüentemente, é desnecessário chamar diretamente essa função, pois várias alternativas de nível
  superior como $ .get () e .load () estão disponíveis e são mais fáceis de usar. Se opções menos comuns forem necessárias, $ .ajax () pode ser usado de forma mais flexível.

  >> O objeto jQuery XMLHttpRequest (jqXHR) retornado por $ .ajax () a partir de jQuery 1.5 é um superconjunto do objeto XMLHttpRequest nativo do navegador. Por exemplo, ele contém 
  as propriedades responseText e responseXML, bem como um método getResponseHeader (). Quando o mecanismo de transporte é algo diferente de XMLHttpRequest (por exemplo, uma tag de script
  para uma solicitação JSONP), o objeto jqXHR simula a funcionalidade XHR nativa onde possível.

  >> Os objetos jqXHR retornados por $ .ajax () a partir do jQuery 1.5 implementam a interface Promise, fornecendo a eles todas as propriedades, métodos e comportamento de uma Promise 
  (consulte Objeto diferido para obter mais informações). Esses métodos usam um ou mais argumentos de função que são chamados quando a solicitação $ .ajax () termina. Isso permite que
  você atribua vários retornos de chamada em uma única solicitação e até mesmo atribua retornos de chamada após a conclusão da solicitação. (Se a solicitação já estiver concluída, o
  retorno de chamada será acionado imediatamente.) Os métodos Promise disponíveis do objeto jqXHR incluem: 

  >> A referência this em todos os callbacks é o objeto na opção de contexto passada para $ .ajax nas configurações; se o contexto não for especificado, esta é uma referência às
  próprias configurações do Ajax.
  */
  jQuery.ajax( url [, settings ] )
  //url      -> String contendo a URL na qual requisição é enviada 
  //settings ->  um objeto plano ({},{'email':'jquery@teste.com'}) que configura a requisição ajax
  
  
 // ------ atributos possiveis para o Plain Object de settings -----

/*
  # accepts [type: Plain Object] (default: depends on dataType) -> Este cabeçalho informa ao servidor que tipo de resposta ele aceitará em troca. Por exemplo, o seguinte define um 
  tipo personalizado mycustomtype a ser enviado com a solicitação:
  */
 $.ajax({
    accepts: {
      mycustomtype: 'application/x-some-custom-type'
    },
   
    // Instructions for how to deserialize a `mycustomtype`
    converters: {
      'text mycustomtype': function(result) {
        // Do Stuff
        return newresult;
      }
    },
   
    // Expect a `mycustomtype` back from server
    dataType: 'mycustomtype'
  });

  /*
    # async [type: boolean] (default: true) -> Se você precisar de solicitações síncronas, defina esta opção como false. Solicitações de domínio cruzado e dataType: solicitações "jsonp" 
    não oferecem suporte à operação síncrona
  */

  /*
  # beforeSend [Type: Function( jqXHR jqXHR, PlainObject settings )] ->Uma função de retorno de chamada de pré-solicitação que pode ser usada para modificar o objeto jqXHR (em jQuery 1.4.x,
    XMLHTTPRequest) antes de ser enviado. Use isto para definir cabeçalhos personalizados, etc. Os objetos jqXHR e settings são passados como argumentos. Este é um evento Ajax. Retornar 
    false na função beforeSend cancelará a solicitação. A partir do jQuery 1.5, a opção beforeSend será chamada independentemente do tipo de solicitação.
  */

  /*
   # cache [typle: boolean] (default: true, false for dataType 'script' and 'jsonp') ->Se definido como falso, ele forçará as páginas solicitadas a não serem armazenadas em cache pelo
    navegador. Observação: definir o cache como falso só funcionará corretamente com solicitações HEAD e GET. Ele funciona anexando "_ = {timestamp}" aos parâmetros GET. O parâmetro
    não é necessário para outros tipos de solicitações, exceto no IE8 quando um POST é feito para uma URL que já foi solicitada por um GET.
  */ 
 
  /*
   # complete [Type: Function( jqXHR jqXHR, String textStatus )] -> Uma função a ser chamada quando a solicitação termina (após a execução de callbacks de sucesso e erro). A função 
   recebe dois argumentos: O objeto jqXHR (em jQuery 1.4.x, XMLHTTPRequest) e uma string categorizando o status da solicitação
   ("sucesso", "não modificado", "nocontent", "erro", "tempo limite", " abortar "ou" parsererror "). A partir do jQuery 1.5, a configuração completa pode aceitar uma série de funções. 
   Cada função será chamada sucessivamente. Este é um evento Ajax.
  */

  /*
   # contents [type: PlainObject] -> Um objeto de pares string / expressão regular que determina como o jQuery analisará a resposta, dado seu tipo de conteúdo. (versão adicionada: 1.5)
  */

  /*
   # contentType [type: boolean or string] (default: 'application/x-www-form-urlencoded; charset=UTF-8') ->Ao enviar dados para o servidor, use este tipo de conteúdo. 
   O padrão é "application / x-www-form-urlencoded; charset = UTF-8", o que é adequado para a maioria dos casos. Se você passar explicitamente um tipo de conteúdo para $ .ajax (),
   ele sempre será enviado ao servidor (mesmo se nenhum dado for enviado). A partir do jQuery 1.6, você pode passar false para dizer ao jQuery para não definir nenhum cabeçalho de 
   tipo de conteúdo. Nota: A especificação W3C XMLHttpRequest determina que o conjunto de caracteres é sempre UTF-8; especificar outro conjunto de caracteres não forçará o navegador
    a alterar a codificação. Nota: Para solicitações de domínio cruzado, configurar o tipo de conteúdo para qualquer coisa diferente de
     application / x-www-form-urlencoded, multipart / form-data ou text / plain irá acionar o navegador para enviar uma solicitação preflight OPTIONS para o servidor.
  */


  /*
   # context [type: PlainObject] ->Este objeto será o contexto de todos os retornos de chamada relacionados ao Ajax. Por padrão, o contexto é um objeto que representa as
   configurações Ajax usadas na chamada ($ .ajaxSettings mescladas com as configurações passadas para $ .ajax). Por exemplo, especificar um elemento DOM como o contexto tornará
   esse o contexto para o retorno de chamada completo de uma solicitação, assim:
  */
    $.ajax({
        url: "test.html",
        context: document.body
    }).done(function() {
        $( this ).addClass( "done" );
    });

 /*
  # converters [type: PlainObject] (default: {"* text": window.String, "text html": true, "text json": jQuery.parseJSON, "text xml": jQuery.parseXML}) ->Um objeto que contém 
  conversores de tipo para dados. Cada valor do conversor é uma função que retorna o valor transformado da resposta.
 */

 /*
  # crossDomain [type: boolean] (default: false for same-domain requests, true for cross-domain requests) -> Se você deseja forçar uma solicitação crossDomain (como JSONP)
   no mesmo domínio, defina o valor de crossDomain como true. Isso permite, por exemplo, o redirecionamento do lado do servidor para outro domínio. (versão adicionada: 1.5)
 */

 /*
  # data [Type: PlainObject or String or Array] ->  Dados a serem enviados ao servidor. Se o método HTTP não pode ter um corpo de entidade, como GET, os dados são anexados ao URL.
Quando os dados são um objeto, o jQuery gera a string de dados dos pares de chave / valor do objeto, a menos que a opção processData seja definida como false. Por exemplo, 
{a: "bc", d: "e, f"} é convertido na string "a = bc & d = e% 2Cf". Se o valor for uma matriz, o jQuery serializa vários valores com a mesma chave com base no valor da configuração 
tradicional (descrito abaixo). Por exemplo, {a: [1,2]} torna-se a string "a% 5B% 5D = 1 & a% 5B% 5D = 2" com a configuração padrão tradicional: falso.

Quando os dados são passados ​​como uma string, eles já devem estar codificados usando a codificação correta para contentType, que por padrão é application / x-www-form-urlencoded.
Em solicitações com dataType: "json" ou dataType: "jsonp", se a string contiver um ponto de interrogação duplo (??) em qualquer lugar no URL ou um único ponto de interrogação (?) 
Na string de consulta, ele será substituído por um valor gerado por jQuery que é exclusivo para cada cópia da biblioteca na página (por exemplo, jQuery21406515378922229067_1479880736745).
 */

 /*
  # dataFilter [Type: Function( String data, String type ) => Anything] -> Uma função a ser usada para manipular os dados de resposta brutos de XMLHttpRequest. Esta é uma função
  de pré-filtragem para higienizar a resposta. Você deve retornar os dados higienizados. A função aceita dois argumentos: os dados brutos retornados do servidor e o parâmetro 'dataType'.
 */

 /*
  # dataType [type: string] (default: Intelligent Guess (xml, json, script, or html)) -> O tipo de dados que você espera do servidor. Se nenhum for especificado,
  o jQuery tentará inferi-lo com base no tipo MIME da resposta. Os tipos disponiveis são:
  - xml
  - html
  - script -> Avalia a resposta como JavaScript e a retorna como texto simples
  - json
  - jsonp
  - text -> uma string de texto simples
 */

 /*
  # error [Type: Function( jqXHR jqXHR, String textStatus, String errorThrown )] -> Uma função a ser chamada se a solicitação falhar
 */

 /*
  # global [type: boolean] (default: true) -> Se deve acionar manipuladores de eventos Ajax globais para esta solicitação. O padrão é verdadeiro. Defina como false para evitar
  que os manipuladores globais como ajaxStart ou ajaxStop sejam acionados. Isso pode ser usado para controlar vários eventos Ajax.
 */

 /*
  # headers [type: PlainObject] (default: {}) -> Um objeto de pares chave / valor de cabeçalho adicionais para enviar junto com solicitações usando o transporte XMLHttpRequest. 
  O cabeçalho X-Requested-With: XMLHttpRequest é sempre adicionado, mas seu valor XMLHttpRequest padrão pode ser alterado aqui. Os valores na configuração dos cabeçalhos também podem ser
   substituídos na função beforeSend. (versão adicionada: 1.5)
 */

 /*
  # ifModified [type: boolean] (default: false) -> Permita que a solicitação seja bem-sucedida apenas se a resposta tiver mudado desde a última solicitação. Isso é feito verificando
  o cabeçalho Last-Modified. O valor padrão é falso, ignorando o cabeçalho.
 */

 /*
  # isLocal[type: boolean] (default: depends on current location protocol) -> Permitir que o ambiente atual seja reconhecido como "local" (por exemplo, o sistema de arquivos), mesmo
   se o jQuery não o reconhecer como tal por padrão. Os seguintes protocolos são atualmente reconhecidos como locais: file, * -extension e widget. Se a configuração isLocal precisar 
   de modificação, é recomendável fazer isso uma vez no método $ .ajaxSetup ()
 */

 /*
  # jsonp [Type: String or Boolean] -> Substitua o nome da função de retorno de chamada em uma solicitação JSONP. Este valor será usado em vez de 'callback' no 'callback =?' parte da
   string de consulta no url. Portanto, {jsonp: 'onJSONPLoad'} resultaria em 'onJSONPLoad =?' passado para o servidor. A partir do jQuery 1.5, definir a opção jsonp como false evita que
    o jQuery adicione a string "? Callback" ao URL ou tente usar "=?" para transformação. Nesse caso, você também deve definir explicitamente a configuração jsonpCallback. Por exemplo,
     {jsonp: false, jsonpCallback: "callbackName"}. Se você não confiar no destino de suas solicitações Ajax, considere definir a propriedade jsonp como false por motivos de segurança.
 */

 /*
 # jsonpCallback [Type: String or Function()] -> Especifique o nome da função de retorno de chamada para uma solicitação JSONP. Este valor será usado em vez do nome aleatório gerado
  automaticamente pelo jQuery. É preferível permitir que o jQuery gere um nome exclusivo, pois isso tornará mais fácil gerenciar as solicitações e fornecer retornos de chamada e tratamento
  de erros. Você pode querer especificar o retorno de chamada quando quiser habilitar um melhor cache do navegador de solicitações GET. A partir do jQuery 1.5, você também pode usar uma
  função para essa configuração, caso em que o valor de jsonpCallback é definido como o valor de retorno dessa função.
 */

 /*
  # method [type: string] (default: 'GET') -> O método HTTP a ser usado para a solicitação (por exemplo, "POST", "GET", "PUT")
 */

 /*
  # mimeType [type: string] -> Um tipo MIME para substituir o tipo MIME XHR
 */

 /*
  # password [type: string] -> Uma senha a ser usada com XMLHttpRequest em resposta a uma solicitação de autenticação de acesso HTTP.
 */

 /*
  # processData [type: boolean] (default: true) -> Por padrão, os dados passados para a opção de dados como um objeto (tecnicamente, qualquer coisa diferente de uma string)
  serão processados e transformados em uma string de consulta, ajustando-se ao tipo de conteúdo padrão "application / x-www-form-urlencoded" . Se você deseja enviar um DOMDocument ou
  outros dados não processados, defina esta opção como false.
 */

 /*
  # scriptAttrs [Type: PlainObject] -> Define um objeto com atributos adicionais a serem usados em uma solicitação de "script" ou "jsonp". A chave representa o nome do atributo e o 
  valor é o valor do atributo. Se este objeto for fornecido, ele forçará o uso de um transporte script-tag. Por exemplo, isso pode ser usado para definir atributos nonce, integridade 
  ou crossorigin para satisfazer os requisitos da Política de Segurança de Conteúdo
 */

 /*
  # scriptCharset [type: string] -> Aplica-se apenas quando o transporte "script" é usado. Define o atributo charset na tag de script usada na solicitação. Usado quando o conjunto de
   caracteres na página local não é o mesmo do script remoto. Como alternativa, o atributo charset pode ser especificado em scriptAttrs, o que também garantirá o uso do transporte "script".
 */

 /**
  # statusCode [type: PlainObject] (default: {}) -> Um objeto de códigos e funções HTTP numéricos a ser chamado quando a resposta possui o código correspondente. Por exemplo, o seguinte
  alertará quando o status da resposta for 404:
  */
  $.ajax({
    statusCode: {
      404: function() {
        alert( "page not found" );
      }
    }
  });

  /*
   # success [Type: Function( Anything data, String textStatus, jqXHR jqXHR )] -> Uma função a ser chamada se a solicitação for bem-sucedida. A função recebe três argumentos: Os dados
   retornados do servidor, formatados de acordo com o parâmetro dataType ou a função de retorno de chamada dataFilter, se especificada; uma string que descreve o status; e o objeto
    jqXHR (em jQuery 1.4.x, XMLHttpRequest). A partir do jQuery 1.5, a configuração de sucesso pode aceitar uma série de funções. Cada função será chamada sucessivamente. Este é um evento Ajax.
  */

  /*
   # timeout [type: number] -> Defina um tempo limite (em milissegundos) para a solicitação. Um valor 0 significa que não haverá tempo limite.
  */

  /*
   # traditional [type: boolean] -> Defina como true se desejar usar o estilo tradicional de serialização de parâmetros. 
  */

  /*
   # type [type: string] (default: 'GET') -> Um apelido para método. Você deve usar type se estiver usando versões do jQuery anteriores a 1.9.0.
  */

  /*
   # url [type: string] (default: The current page) -> Uma string contendo o URL para o qual a solicitação é enviada.
  */

  /*
   # username [type: string] -> Um nome de usuário a ser usado com XMLHttpRequest em resposta a uma solicitação de autenticação de acesso HTTP.
  */

  /*
   # xhr [type: function] (default: ActiveXObject when available (IE), the XMLHttpRequest otherwise) -> Retorno de chamada para criar o objeto XMLHttpRequest. O padrão é ActiveXObject 
   quando disponível (IE), o XMLHttpRequest caso contrário. Substitua para fornecer sua própria implementação para XMLHttpRequest ou aprimoramentos para a fábrica.
  */

  /*
   # xhrFields [type: PlainObject] -> Um objeto de pares fieldName-fieldValue para definir no objeto XHR nativo. Por exemplo, você pode usá-lo para definir withCredentials como true para
    solicitações entre domínios, se necessário.
  */
  $.ajax({
    url: a_cross_domain_url,
    xhrFields: {
      withCredentials: true
    }
  });

  /*
   # 
  */
});