$(() => {

    //forma recomendada de usar eventos
    $(".btn").on("evento [,selector][,data]", function(){
        
    });

    $(".btn").on("click", function(){

    });

    //Cada input criado receberá os estilos 'desfocado' e 'focado'
    //A ideia aqui e que os elementos pai, neste caso (body), forneceram para os filhos, neste caso (input) acesso aos eventos.
    $('body').on('focus', 'input' ,e =>{
        $(e.target).removeClass('desfocado')
        $(e.target).addClass('focado')
    })

    $('body').on('blur','input', e =>{
        $(e.target).removeClass('focado')
        $(e.target).addClass('desfocado')
    })

    //removendo um evento.
	$('body').off('blur','input')
})