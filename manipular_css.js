$(() => {
    
    //Adicionando classes a  elementos html. Use somente o nome da classe
    $('input').addClass('campo')
    $('textarea').addClass('novo')

    //removendo classe
    $('input').removeClassClass('campo')

    //Se a classe não existe é adicionada, caso ja exista e removida
    $('input').toggleClass('campo')

    //Testando a existencia de uma classe
	$('input').hasClass('campo')//retorna true ou false

    //retorna a cor do elemento
    $("p").css("background-color");

    //setando novos valores
    $("p").css("background-color", "yellow");

    //ao definir mais de um propriedade, deve-se passar um objeto
    $("p").css({
        "background-color": "yellow",
         "font-size": "200%"
    });
    //ou
    $("p").css({
        backgroundColor: "yellow",
        fontSize: "200%"
    });
    



});