/*
# Equivalentes
JQuery === $
JQuery.each() === $.each()

# arrow functions não mantem o escopo da função.

# retorna um array de todos so campos digitados
$(this).serializeArray();

# onde o evento ocorreu, onde foi clicado (por exemlo exemplo, <span>)
event.target

# onde oe evento foi definido (neste exemplo, <p>)
event.currentTarget

# data => Objeto que permite ser criados atributos dinamicos no DOM
<input type="text" data-user="logado">

# PlainObject (Objeto plano)
Um objeto contendo zero ou masi pares chave-valor
{}, {"nome":"Marina"}, ...

# Evitar comportamento padrão 
event.preventDefault()


*/