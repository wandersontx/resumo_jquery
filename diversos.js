$(() => {

  //simula um evento
  $('#register :checkbox').trigger('click')

  //Executando um evento apenas uma vez
  $('.books').append('<ul>Marque seus favoritos (essa ação não pode ser desfeita)</ul>')
  $('#books_list tbody tr').one('click', function(){        
      if(!$(this).hasClass('selected')) {
          $(this).addClass('trselected')
      } else {
          $(this).removeClass('trselected')
      }
  });

  //crirando um namespace de evento
  $('p').on('click.colorRed', function(){
    $(this).toggleClass('selected_section');
  });
  
  //desavatianod evento
  $('#btn_remove_css_color').on('click', function(){
  $('p').off('click.colorRed'); 
  });


})