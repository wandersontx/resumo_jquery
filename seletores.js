/*
   $(document).ready(function(){

   });

   //forma mais recente de declarar o metodo readt
   $(()=>{

   })
*/

//Lembrando que se pode combinar essas formas abaixo para selecionar um ou mais elementos
$(document).ready(function(){


   //selecionando elemento
   $("body")

   //selecioando por tag
   $("h1")

   //seleção por ID
   $("#my-id")

   //seleção por classe
   $(".my-class")

   //seleção css composto (ul com id lista e selecioando uma li que tenha a classe ativo)
   $("#lista li.ativo")
   $('header nav.menu')

   //multiplos seletores
   $("div.azul, li.verde")

   //atributos. São selecionados a partir do [] 
   $("input[name=telefone]")
   $(".header-border li [href][title]")
   $('.header-border li [href="about.html"]') //a partir do valor 'about.html'
   $('.header-border li [href^="i"]') //equivale a 'i%' do sql
   $('.header-border li [href$="i"]') //equivale a '%i' do sql
   $('.header-border li [href*="i"]') //equivale a '%i%' do sql   

   //filtrando o primeiro elemento
   $(".cars_list li:first")

   //filtrando ultimo elemento
   $(".cars_list li:last")  

   //filtrando de forma alternada, impares 1,3,5...
   $(".cars_list li:even")

   //filtrando itens nas posições pares 2,4,6...
   $("tr:odd")

   //selecionando uma posição especifica
   $(".cars_list li:eq(2)")

   //seleciona todos elementos onde o indice e maior do que informado entre parentese
   $("div:gt(2)")

   //seleciona todos antes de uma posição informada
   $(".cars_list li:lt[2]")
  
   //seleciona todos elementos que foram checados
   $("#formulario :checked")

   //selecionando tag com valor vazio
   $('.cars_list li:empty')

   //filtrando  tag que contenha algum valor
   $('.cars_list li:parent')

   //selecionando elementos ocultods
   $('.cars_list li:hidden')

   //seleciona todos elementos que estão visiveis
   $("li:visible")

   //selecionar um descendente, basta informar o 'filho' apos espaço
   $("main h1")

   //selecionando uma tag descendente direta
   $("main>.title")

   //selecionando a 'p' mais proxima da classe paragrafo.
   $(".paragrafo+p")

   //selecionando todas a tag 'p'(selecionando tag irmã)
   $(".paragrafo~p")

   //selecioando um propriedade disabled..testando no console
   $('#register :disabled').prop('disabled', false)


});


